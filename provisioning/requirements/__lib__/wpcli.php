<?php

namespace Provisioning\Requirements;

use WP_CLI;

define('LIB_DIR', __DIR__ . DIRECTORY_SEPARATOR);
define('ROOT_DIR', dirname(__DIR__) . DIRECTORY_SEPARATOR);

require_once LIB_DIR . 'Dependency.php';
require_once LIB_DIR . 'DependenciesCollection.php';

const CMD_OPTIONS = array(
	'return'     => false,
	'exit_error' => false,
	'launch'     => false,
);

require_once ROOT_DIR . 'plugins.php';
const PLUGIN_INSTALL_CMD  = 'plugin install %s %s';
const PLUGIN_ACTIVATE_ARG = '--activate';

function install_plugins() {
	plugins()->forEach( function ( Dependency $plugin ) {
		if ( WP_DEBUG ) {
			WP_CLI::runcommand( sprintf(
				PLUGIN_INSTALL_CMD,
				$plugin->name,
				$plugin->development ? PLUGIN_ACTIVATE_ARG : '',
			), CMD_OPTIONS );
		} elseif ( $plugin->production ) {
			WP_CLI::runcommand( sprintf(
				PLUGIN_INSTALL_CMD,
				$plugin->name,
				PLUGIN_ACTIVATE_ARG,
			), CMD_OPTIONS );
		}
	} );
}


require_once ROOT_DIR . 'themes.php';
const THEME_INSTALL_CMD = 'theme install %s';

function install_themes() {
	themes()->forEach( function ( Dependency $theme ) {
		WP_CLI::runcommand( sprintf( THEME_INSTALL_CMD, $theme->name ), CMD_OPTIONS );
	} );
}

function install() {
	install_plugins();
	install_themes();
}