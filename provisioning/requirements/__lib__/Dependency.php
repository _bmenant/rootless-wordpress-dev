<?php

namespace Provisioning\Requirements;

class Dependency {
	public string $name;
	public bool $development = true;
	public bool $production = true;

	public function __construct( string $name ) {
		$this->name = $name;
	}

	/**
	 * Install and activate the dependency on production and development (by default).
	 * @return $this
	 */
	public function forBoth(): self {
		$this->development = true;
		$this->production  = true;

		return $this;
	}

	/**
	 * Install and activate the dependency on development only. The dependency is NOT installed on production.
	 *
	 * @return $this
	 */
	public function forDevOnly(): self {
		$this->production  = false;
		$this->development = true;

		return $this;
	}

	/**
	 * Install the dependency on production and development, but activate it on production only.
	 *
	 * @return $this
	 */
	public function forProdOnly(): self {
		$this->production  = true;
		$this->development = false;

		return $this;
	}
}

function new_dependency( string $name ): Dependency {
	return new Dependency( $name );
}