<?php

namespace Provisioning\Requirements;

class DependenciesCollection {
	/**
	 * @var Dependency[]
	 */
	private array $dependencies;

	/**
	 * @param Dependency[] $dependencies
	 */
	public function __construct( array $dependencies = array() ) {
		$this->dependencies = $dependencies;
	}

	/**
	 * @param callable $fn takes a {@see Dependency}
	 */
	public function forEach( callable $fn ): void {
		array_map( $fn, $this->dependencies );
	}
}