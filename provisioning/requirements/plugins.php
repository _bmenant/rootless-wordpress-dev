<?php
namespace Provisioning\Requirements;

function plugins(): DependenciesCollection {
	return new DependenciesCollection( [
		new_dependency('show-hooks')->forDevOnly(),
		new_dependency('wordpress-importer')->forDevOnly(),
		//new_dependency('acf'),
		//new_dependency('mailin')->forProdOnly(),
	] );
}

