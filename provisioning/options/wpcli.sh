#!/bin/sh

apply_wp_options() {

  apply_option timezone_string "Europe/Paris"
  apply_option time_format "H:i"
  apply_option date_format "Y-m-d"
  apply_option blog_public 0

  wp --skip-plugins --skip-themes --url="https://localhost" rewrite structure "/%postname%/"

}

apply_option() {
  wp --skip-plugins --skip-themes option update "$1" "$2"
}

