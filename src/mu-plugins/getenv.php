<?php
/*
Plugin Name: WordPress GetEnv for Production
Plugin URI: https://framagit.org/ape/wp-platform-action
Description: Provides getenv_config function for both containerized and classic host runtimes
Author: Benjamin Menant
Version: 1.0.0
*/

if ( ! function_exists( 'getenv_config' ) ) {
	function getenv_config( $env, $default ) {
		if ( wp_get_environment_type() === 'development' ) {
			return getenv_docker( $env, $default );
		}

		$fileEnv = realpath( dirname( ABSPATH ) . '/config/' . $env . '_PRODUCTION' );

		return $fileEnv
			? rtrim( file_get_contents( $fileEnv ) )
			: $default;
	}
}
