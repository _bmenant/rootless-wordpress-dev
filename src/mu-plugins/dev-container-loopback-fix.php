<?php
/*
Plugin Name: WordPress Loop-back Fix for Container
Plugin URI: https://framagit.org/ape/wp-platform-action
Description: Fixes WordPress loop-back requests in container running with tcp port published with a translation (i.e. 443:8443). Use LOCAL_HTTPS_PORT environment variable (default to 8443).
Author: Benjamin Menant
Version: 1.0.0
*/

if ( wp_get_environment_type() === 'development' ) {
	add_filter('site_url', function ($url, $path) {
		$local_https_port = (int) getenv_config('LOCAL_HTTPS_PORT', 8443);
		$needle = 'wp-cron.php';
		if (strncmp($path, $needle, strlen($needle)) === 0) {
			return sprintf('https://localhost:%d/%s', $local_https_port, $needle);
		}
		return $url;
	}, 10, 2);
}
