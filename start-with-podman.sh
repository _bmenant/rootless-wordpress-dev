#!/bin/sh

# Show help if --help argument was given
echo " ${@} " | grep -qe ' --help \| -[a-zA-Z]*h[a-zA-Z]* '
if [ $? -eq 0 ]; then
  echo "Usage: start-with-podman.sh [OPTION]... [NAME]"
  echo "Build and start a rootless pod to run a complete WordPress \
development stack." | fold -w 80 -s
  echo "Example: start-with-podman.sh --update hello-world"
  echo "Use NAME to prefix pod and containers names. If NAME is omitted, the \
name of the directory containing this script is used instead." | fold -w 80 -s
  echo
  echo -e "  -h, --help\t\tDisplay this help text and exit"
  echo -e "  -u, --update\t\tPull latest wordpress image, rebuild and restart"
  exit 0
fi

get_container_name() {
  local container_name=$(echo " ${@} " | grep -oe ' [a-zA-Z_][a-zA-Z_-]* $')
  if [ "$container_name" ]; then
    echo $(echo "$container_name" | tr -d ' ')
  else
    echo $(basename $(dirname $(realpath "$0")))   
  fi
}

do_update_wordpress_image() {
  echo " ${@} " | grep -qe ' --update \| -[a-zA-Z]*u[a-zA-Z]* '
  if [ $? -eq 0 ]; then

    # Rebuild image from latest upstream wordpress image
    podman pull docker.io/library/wordpress:php8.0
    do_build_container

    # Remove WordPress sources; will be repopulated on container’s first start with up-to-date sources
    rm -rf ./src/.wp/*

    podman stop --ignore "${CONTAINER_NAME}-web"
    podman rm --ignore "${CONTAINER_NAME}-web"
    if podman pod exists "${CONTAINER_NAME}-pod"; then
      do_create_container
    fi
  fi
}

do_build_container() {
  podman build -t "${CONTAINER_NAME}-wordpress:podman" ./container/wp
}

do_create_container() {
  podman create \
    --pod="${CONTAINER_NAME}-pod" \
    --name="${CONTAINER_NAME}-web" \
    --user="$(id -u):$(id -g)" \
    --volume=./src/plugins:/var/www/wp/wp-content/plugins:Z \
    --volume=./src/mu-plugins:/var/www/wp/wp-content/mu-plugins:Z \
    --volume=./src/themes:/var/www/wp/wp-content/themes:Z \
    --volume=./container/data/wp-uploads:/var/www/wp/wp-content/uploads:Z \
    --volume=./src/.wp:/var/wp:Z \
    --volume=./container/ssl:/etc/apache2/ssl:ro,Z \
    --volume=./provisioning/requirements:/var/wp-requirements:ro,Z \
    --volume=./provisioning/options:/var/wp-options:ro,Z \
    --env-file=.env_web \
    --env=WORDPRESS_DB_HOST=127.0.0.1 \
    --env=WORDPRESS_DB_USER=wordpress \
    --env=WORDPRESS_DB_NAME=wordpress \
    --env=WORDPRESS_DB_PASSWORD=wordpresssecret \
    --env=WORDPRESS_DEBUG=true \
    --env=WORDPRESS_CONFIG_EXTRA="define( 'WP_ENVIRONMENT_TYPE', 'development' ); define( 'WP_AUTO_UPDATE_CORE', false ); \$_SERVER['HTTPS'] = 'on'; \$_SERVER['SERVER_PORT'] = 443;" \
    --env=PHP_IDE_CONFIG="serverName=host.containers.public" \
    "${CONTAINER_NAME}-wordpress:podman"
}

# Use container name given with command-line argument, or script’s parent directory name
CONTAINER_NAME=$(get_container_name "$@")

# Update if --update argument was given
do_update_wordpress_image "$@"

if podman pod ps --filter status=running --noheading | grep -q "'${CONTAINER_NAME}-pod'"
then echo "'${CONTAINER_NAME}-pod' is already running"

elif podman pod exists "${CONTAINER_NAME}-pod"
then podman pod start "${CONTAINER_NAME}-pod"

else

  podman pod create \
    --publish=443:8443 \
    --publish=8025:8025 \
    --publish=3306:3306 \
    --add-host="host.containers.public:$(hostname -I | cut -f1 -d' ')" \
    --userns=keep-id \
    --name="${CONTAINER_NAME}-pod"

  if ! podman image exists rootless-wordpress:podman
  then do_build_container
  fi

  if ! podman container exists "${CONTAINER_NAME}-web"
  then do_create_container
  fi

  podman rm --ignore "${CONTAINER_NAME}-smtp"
  podman create \
    --pod="${CONTAINER_NAME}-pod" \
    --name="${CONTAINER_NAME}-smtp" \
    docker.io/cd2team/mailhog

  podman rm --ignore "${CONTAINER_NAME}-db"
  podman create \
    --pod="${CONTAINER_NAME}-pod" \
    --name="${CONTAINER_NAME}-db" \
    --volume=./container/data/.dbdata:/var/lib/mysql:Z \
    --volume=./provisioning/sqldump:/docker-entrypoint-initdb.d:ro,Z \
    --env=MYSQL_DATABASE=wordpress \
    --env=MYSQL_USER=wordpress \
    --env=MYSQL_PASSWORD=wordpresssecret \
    --env=MYSQL_ROOT_PASSWORD=wordpressverysecret \
    docker.io/library/mariadb:10

  podman pod start "${CONTAINER_NAME}-pod"

fi

