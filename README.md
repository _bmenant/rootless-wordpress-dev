# WordPress Rootless Container for Development

## Getting started

This project may (should) work as a rootless containerized development stack with a WordPress instance.
Using Podman, it creates a pod with three containers `web` (Apache2 & PHP), `db` (MariaDB) and `smtp` (Mailhog).

### 1. Prerequisites

- [Podman](https://podman.io/) v3.4+ (or Docker/Mobi with docker-compose).
- Capability to bind to port 443 (HTTPS) to go rootless.
  Either set `CAP_NET_BIND_SERVICE` to Podman, or lower unprivileged port start:
  `sudo sysctl net.ipv4.ip_unprivileged_port_start=443`

### 2. Setup your own SSL certificate

Create a self-signed certificate (consider using [mkcert](https://mkcert.dev/) to setup a local certificate authority),
then copy the certificate file to `./container/ssl/cert.pem` and the key file to `./container/ssl/cert-key.pem`.

### 3. Seed the database (optional)

Copy your (gziped) sql file under `./provisioning/sqldump`.

**⚠ Database files are persisted under `./container/.dbdata`** for subsequent executions.
Those files are not deleted on container or volume removal. Please delete those files manually if you want to start
the stack from scratch.

### 4. Create a `.env*` files

Optionally, add a file named `.env_web` at the root of the project. This file may be used to define environment variables
for the WordPress instance, such as plugins configurations. See dedicated sections below for details.

#### 👉 Using docker-compose

For docker-compose, add a `.env` file with the following variables: 

```dotenv
# UID must be your user id (run `id -u` in your terminal if you’re unsure)
UID=<integer>
# GID must be your group id (run `id -g` in your terminal if you’re unsure)
GID=<integer>
# Host IP address rootable from containers; probably the first local public IP (run `hostname -I | cut -f1 -d' '` in your terminal if you’re unsure)
HOST_PUBLIC_IP=<ip_address>
```

### 5. Start containers

#### Using Podman

Execute the following shell script with your current user (rootless):

```shell
./start-with-podman.sh hello-world

# And to shut it down...
podman pod stop hello-world-pod
```

This creates a new pod, build the container image, create containers and start the pod.

##### --help

To get some help about this command-line utility:

```shell
./start-with-podman.sh --help
```

#### Using docker-compose

```shell
docker-compose up -d
```

⚠ Make sure `$UID` and `$GID` environment variables are defined! Docker does not substitute shell variables.
If you get warning about those, add a `.env` file at the root of the project (see _section 4._ above for details).

### 6. You’re all set!

- Instance available at https://localhost/.
- WordPress Admin: https://localhost/wp-admin (user: `admin`, password: `admin`).

## Update

Using Podman, simply execute:

```shell
./start-with-podman.sh --update
```

Otherwise, to _manually_ update your local WordPress instance:

1. Pull the latest wordpress image from `docker.io/library/wordpress:php8.0`;
2. Rebuild the container image;
3. Stop and **remove** the container;
4. Remove WordPress sources: `rm -rf ./src/.wp/*`;
5. Restart the container.

## Development Utils

### XDebug 3

XDebug will init connections on your host (default port 9003).

Set up your IDE with a the following settings:

* Server Name: `host.containers.public`
* Host Name: `localhost`
* TCP Port: `8443`
* Path Mappings:
  * `src/.wp` → `/var/www/wp`
  * `src/.wp/wp-cli.phar` → `/usr/local/bin/wp`
  * `src/mu-plugins` → `/var/www/wp/wp-content/mu-plugins`
  * `src/plugins` → `/var/www/wp/wp-content/plugins`
  * `src/themes` → `/var/www/wp/wp-content/themes`

💡 You may (have to) create an entry for every hostname you use.

#### Activate

XDebug mode is set to `trigger`. To start debugging, you may trigger XDebug 
with a [browser addons](https://www.jetbrains.com/help/phpstorm/2022.1/browser-debugging-extensions.html).

### Logs

```shell
# Apache & PHP
podman logs name_of_your_container-web

# MariaDB
podman logs name_of_your_container-db
```

### WP-CLI

You can exec [WP-CLI commands](https://developer.wordpress.org/cli/commands/)
using `podman exec web wp`

#### Example: Migrate the DB from production’s domain to localhost

```shell
podman exec -ti name_of_your_container-web wp \
search-replace 'www.example.com' 'localhost' \
--recurse-objects \
--skip-columns=guid \
--skip-tables=wp_users 
```

### Emails

All emails sent from the Wordpress instance (using SMTP) are caught by a Mailhog container. You can see all sent emails in the
Mailhog web UI at http://localhost:8025.

### MariaDB

The MariaDB instance is available from `localhost` under standard MySQL TCP port `3306` (user: `wordpress`, base: `wordpress`, password: `wordpresssecret`).

### WordPress core sources

WordPress core files (i.e. php scripts under wp-includes and wp-admin) are made available under the `./.wp` directory.

💡 IDE (e.g. PhpStorm, Visual Studio) may be set up to use this directory as WordPress sources.

**Notice:** those sources are mere plain copies and are **not** used at runtime.

### Uploads

Media and files uploaded to the WordPress instance are stored in the `./container/wp-uploads/` directory.

💡 Those files are not deleted on container or volume removal.

## Plugins and Themes Development

### Git

By default, Git ignores themes and plugins (we don’t want to stage upstream plugins and themes). 

⚠ Therefore, please **explicitly add any new custom theme or plugin** to `.gitignore`:

```gitignore
# At the bottom of the file
!src/plugins/plugin-to-include
!src/themes/theme-to-include
```

### Requirements (plugins and themes pre-installation)

See `./provisioning/requirements/plugins.php` and `./provisioning/requirements/themes.php` to declare 
upstream plugins and themes to be installed and activated at pod creation.

🚀 Those requirements are used for production deployments as well. 

### Plugins and Themes

Upstream plugins and themes can be auto-updated, or manually updated from the WordPress admin or using WP-CLI.

Alternatively, you could remove unstaged files under `./src/themes` and `./src/plugins`, and restart the container.
Then entrypoint script will install latest version (see `./provisioning/requirements`).

### Environment Variables

💡 Environment variables defined in `.env_web` are exported within the web container.

See section below for a detailed list: _Deployments - Environment Variables_.

### Configuration

If a theme or a plugin needed specific configuration (as environment variables),
use the global function `getenv_config( string $env, mixed $default ) : string`.

Please set **sensible default for development** environment, while **not** exposing secrets (such as passwords or private keys).
Those can be defined locally per user in `.env_web`.

⚠ Make sure to update the Environment Variable section in this documentation.

🚀 Make sure to create the variables for production deployments (see Deployments section).

## Deployments

🚧 EXPERIMENTAL 🚧

Gitlab CI deploys public assets, (mu-)plugins and themes sources to the production server, using SFTP.
Deployments automatically start when new commits are merged into to the `production` branch.

**Notice:** only changed files are uploaded. Deleted files are not removed from the remote host.

**Caution:** (new) plugins and themes are **not** enabled (nor installed as of today)!

### Support

- [x] Changed files in `./public/`
- [x] Changed files in `./src/themes/`
- [x] Changed files in `./src/plugins/`
- [x] Changed files in `./src/mu-plugins/`
- [ ] Plugins declared in requirements (see #1)
- [ ] Themes declared in requirements (see #1)
- [x] Configuration (see below)

### Configuration

In order to deploy configuration (see Environment Variables section below),
_Variables_ have to be defined in Gitlab-CI settings:

For each environment variable, create a _variable_ (type: env) of the same name as the actual environment variable, suffixed with `_PRODUCTION`.
Set the _variable_ as protected and masked (append trailing  dashes`-` if the content is too short to be masked).

For example, for the environment variable `FOOBAR`, create a _variable_ `FOOBAR_PRODUCTION`. 

### Environment Variables

The following environment variables SHOULD be defined: _none_.

### Public Resources

Files stored under `./public` are used for production deployments and shouldn’t serve during development
(files are copied to the root of the HTTP host; relative paths are preserved). 

### New Instance

To bootstrap a production instance from scratch and set up deployments from this project, 
make sure the following requirements are met:

- Fully installed and working WordPress instance.
- Pre-created `config` directory available under `{webroot}/../config`
  where `{webroot}` is the absolute path to your public root directory, 
  served by your HTTP service.
- Pre-created `mu-plugins`, `plugins` and `themes` directories 
  under `{webroot}/wp-content/`.
- Update Gitlab-CI variables.

Standard WordPress recommendations for security and performance applies.
Notably: consider enabling auto-updates for WordPress and upstream themes and plugins.
