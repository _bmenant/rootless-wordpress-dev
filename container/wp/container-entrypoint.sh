#!/usr/bin/env bash
set -Eeuo pipefail

if [[ "$1" == apache2* ]] || [ "$1" = 'php-fpm' ]; then
	uid="$(id -u)"
	gid="$(id -g)"
	if [ "$uid" = '0' ]; then
		case "$1" in
			apache2*)
				user="${APACHE_RUN_USER:-www-data}"
				group="${APACHE_RUN_GROUP:-www-data}"

				# strip off any '#' symbol ('#1000' is valid syntax for Apache)
				pound='#'
				user="${user#$pound}"
				group="${group#$pound}"
				;;
			*) # php-fpm
				user='www-data'
				group='www-data'
				;;
		esac
	else
		user="$uid"
		group="$gid"
	fi

  if [ ! -e wp-content/themes/index.php ]; then
    # if themes directory seems empty, copy default themes (do not overwrite existing files)
    cp -an /usr/src/wordpress/wp-content/themes/* wp-content/themes/
  fi

  if [ ! -e wp-content/plugins/index.php ]; then
    # if plugins directory seems empty, copy default plugins (do not overwrite existing files)
    cp -an /usr/src/wordpress/wp-content/plugins/* wp-content/plugins/
  fi

	if [ ! -e /var/wp/index.php ] && [ ! -e /var/wp/wp-includes/version.php ]; then
	  # if wp directory seems empty, copy WordPress (do not overwrite existing files)
    cp -an /usr/src/wordpress/* /var/wp/
    cp /usr/local/bin/wp /var/wp/wp-cli.phar
  fi

	wpEnvs=( "${!WORDPRESS_@}" )
	if [ ! -s wp-config.php ] && [ "${#wpEnvs[@]}" -gt 0 ]; then
		for wpConfigDocker in \
			wp-config-docker.php \
			/usr/src/wordpress/wp-config-docker.php \
		; do
			if [ -s "$wpConfigDocker" ]; then
				echo >&2 "No 'wp-config.php' found in $PWD, but 'WORDPRESS_...' variables supplied; copying '$wpConfigDocker' (${wpEnvs[*]})"
				# using "awk" to replace all instances of "put your unique phrase here" with a properly unique string (for AUTH_KEY and friends to have safe defaults if they aren't specified with environment variables)
				awk '
					/put your unique phrase here/ {
						cmd = "head -c1m /dev/urandom | sha1sum | cut -d\\  -f1"
						cmd | getline str
						close(cmd)
						gsub("put your unique phrase here", str)
					}
					{ print }
				' "$wpConfigDocker" > wp-config.php

				if [ "$uid" = '0' ]; then
					# attempt to ensure that wp-config.php is owned by the run user
					# could be on a filesystem that doesn't allow chown (like some NFS setups)
					chown "$user:$group" wp-config.php || true
				fi
				break
			fi
		done
	fi

  timeout 90s bash -c "until mysql -h${WORDPRESS_DB_HOST} -u${WORDPRESS_DB_USER} -p${WORDPRESS_DB_PASSWORD} --execute \"SHOW DATABASES;\" > /dev/null ; do sleep 5 ; done"
  if [ $? -eq 124 ] ; then
    #timed out
    echo "Unable to connect MariaDB"
    exit 1
  fi

	if ! wp --skip-plugins --skip-themes core is-installed; then
	  wp core install \
	    --url="https://localhost" \
	    --title="${WORDPRESS_TITLE:-'Rootless WordPress Container'}" \
	    --admin_user=admin \
	    --admin_password=admin \
	    --admin_email=dev@menant-benjamin.fr \
	    --skip-email
	fi

  if [ ! -e .wp-requirements-installed ]; then
    # if requirements (plugins and themes) have not been installed yet (run only on container’s first start)
    wp eval "\Provisioning\Requirements\install();" \
      --skip-plugins \
      --skip-themes \
      --url="https://localhost" \
      --require="/var/wp-requirements/__lib__/wpcli.php"
    if [ $? -eq 0 ]; then
      touch .wp-requirements-installed
    fi
  fi

  if [ ! -e .wp-options-applied ]; then
    # if options (wordpress configuration) have not been applied yet
    . /var/wp-options/wpcli.sh
    apply_wp_options
    if [ $? -eq 0 ]; then
      touch .wp-options-applied
    fi
  fi
fi

exec "$@"

