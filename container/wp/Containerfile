FROM docker.io/library/golang:1 AS mhsendmail
RUN go install github.com/mailhog/mhsendmail@latest

FROM docker.io/library/wordpress:php8.0

# WordPress read-only sources (e.g. wp-includes, wp-admin) will be copied into this volume (to be used with IDE)
VOLUME /var/wp
VOLUME /var/wp-requirements
VOLUME /var/wp-options

EXPOSE 8080
EXPOSE 8443

# Using docker-compose, MariaDB and SMTP services are available under their own domain (e.g. db, smtp).
# Using Podman, all services are created in a pod and are available to each other through localhost.
ARG SMTP_HOST=localhost

WORKDIR /var/www/wp

#
## Sendmail
#

COPY --from=mhsendmail /go/bin/mhsendmail /usr/local/bin/
RUN echo "sendmail_path='/usr/local/bin/mhsendmail --smtp-addr=${SMTP_HOST}:1025'" > /usr/local/etc/php/conf.d/mailhog.ini \
    && chmod a+rx /usr/local/bin/mhsendmail \
    && chown root: /usr/local/bin/mhsendmail /usr/local/etc/php/conf.d/mailhog.ini

#
## SSL
#

RUN    sed -i -e 's/80/8080/g' -e 's/443/8443/g' /etc/apache2/ports.conf \
    && sed -i -e 's/80/8080/g' -e 's/443/8443/g' -e 's/www\/html/www\/wp/g' /etc/apache2/sites-available/*.conf \
    && sed -i \
        -e 's!\(SSLCertificateFile\).*$!\1 /etc/apache2/ssl/cert.pem!g' \
        -e 's!\(SSLCertificateKeyFile\).*$!\1 /etc/apache2/ssl/cert-key.pem!g' \
        /etc/apache2/sites-available/default-ssl.conf \
    && ln -rs /etc/apache2/mods-available/ssl.load /etc/apache2/mods-enabled/ \
    && ln -rs /etc/apache2/mods-available/ssl.conf /etc/apache2/mods-enabled/ \
    && ln -rs /etc/apache2/mods-available/socache_shmcb.load /etc/apache2/mods-enabled/ \
    && ln -rs /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-enabled/

#
## WP-CLI
#

RUN apt-get update \
	&& apt-get install -y less default-mysql-client \
	&& rm -rf /var/lib/apt/lists/*

RUN curl -o /usr/local/bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && chmod a+rx /usr/local/bin/wp \
    && chown root: /usr/local/bin/wp

#
## Browscap
#

RUN curl -o /etc/browscap.ini 'https://browscap.org/stream?q=Lite_PHP_BrowsCapINI' \
    && chmod a-xw /etc/browscap.ini \
    && chown root: /etc/browscap.ini

#
## XDebug
#

RUN pecl install xdebug && docker-php-ext-enable xdebug
COPY php-conf.d/ /usr/local/etc/php/conf.d/

#
## WordPress
#

RUN cp -a /usr/src/wordpress/* /usr/src/wordpress/.htaccess /var/www/wp/ \
    && cp -af /var/www/html/* /var/www/wp/ \
    && chown www-data: /var/www/wp \
    && chmod a+w /var/www/wp

#
## Entrypoint
#

COPY container-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod a+x /usr/local/bin/docker-entrypoint.sh

